;; fsbl2 - homebrew Lisp system
;; Copyright (C) 2018 Robert Cochran <robert-git@cochranmail.com>
;;
;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(defpackage :gen-x86
  (:use :common-lisp)
  (:export :ir->asm
	   :irs->asm
	   :gen-lisp-prologue
	   :gen-lisp-epilogue))

(in-package :gen-x86)

(defconstant +fixnum-shift+ 2)
(defconstant +fixnum-mask+ #x03)

(defconstant +char-shift+ 8)
(defconstant +char-mask+ #x0f)

(defconstant +nil-rep+ #x2f)
(defconstant +t-rep+ #x6f)

(defconstant +bool-shift+ (floor (log (- +t-rep+ +nil-rep+) 2)))

(defconstant +non-immediate-mask+ #x07)

(defconstant +cons-tag+ #x01)

(defconstant +word-size+ 4)

(defconstant +fixnum-bits+ (- (* +word-size+ 8) +fixnum-shift+))
(defconstant +fixnum-min+ (- (expt 2 (1- +fixnum-bits+))))
(defconstant +fixnum-max+ (1- (expt 2 (1- +fixnum-bits+))))

(defun fixnump (x)
  "Return non-NIL if X is a FSBL fixnum."
  (and (integerp x)
       (<= +fixnum-min+ x +fixnum-max+)))

(defun booleanp (x)
  "Return non-NIL if X is a boolean (T or NIL)."
  (or (eq x t)
      (null x)))

(defun charp (x)
  "Return non-NIL if X is an 8bit ASCII character."
      ; SBCL base-char is non-Unicode 8 bit, regardless of support being on
  (or #+sbcl (typep x 'base-char)
      ; Guess that encoding is ASCII or a superset
      #-sbcl (<= 0 (char-code x) 255)
      nil))

(defun immediatep (x)
  "Return non-NIL if X is a FSBL immediate (fixnum or boolean)."
  ;; NIL is considered a bool, so no need to explicitly check for it.
  (or (fixnump x)
      (booleanp x)
      (charp x)))

(defun gen-immediate (immediate)
  "Return the FSBL representation of an immediate IMMEDIATE."
  (unless (immediatep immediate)
    (error "~s is not a FSBL immediate!" immediate))
  (format nil "~2,'0xh"
          (cond ((fixnump immediate) (ash immediate +fixnum-shift+))
                ((booleanp immediate) (if immediate
					  +t-rep+
					  +nil-rep+))
                ((charp immediate) (+ (ash (char-code immediate) +char-shift+)
				      #x0f)))))

(defun gen-lisp-prologue ()
  (format t "	push esi~%")
  (format t "	mov esi, 0~%"))

(defun gen-lisp-epilogue ()
  (format t "	pop esi~%")
  (format t "	ret~%"))

(defun gen-load-immediate (immediate)
  (format t "	mov eax, ~a~%" (gen-immediate immediate)))

(defun gen-jmp (label)
  (format t "	jmp ~a~%" label))

(defun gen-jmp-if-nil (label)
  (format t "	cmp eax, ~a~%" (gen-immediate nil))
  (format t "	jz ~a~%" label))

(defun gen-jmp-if-t (label)
  (format t "	cmp eax, ~a~%" (gen-immediate t))
  (format t "	jz ~a~%" label))

(defun gen-eq ()
  (format t "	mov ecx, DWORD [esp]~%")
  (format t "	cmp ecx, DWORD [esp+~2,'0xh]~%" +word-size+)
  (format t "	sete al~%")
  (format t "	movzx eax, al~%")
  (format t "	sal eax, ~2,'0xh~%" +bool-shift+)
  (format t "	or eax, ~a~%" (gen-immediate nil)))

(defun gen-null ()
  (format t "	cmp DWORD [esp], ~a~%" (gen-immediate nil))
  (format t "	sete al~%")
  (format t "	movzx eax, al~%")
  (format t "	sal eax, ~2,'0xh~%" +bool-shift+)
  (format t "	or eax, ~a~%" (gen-immediate nil)))

(defun gen-cons ()
  (format t "	call alloc_cons~%")
  (format t "	mov ecx, [esp]~%")
  (format t "	mov [eax-~2,'0xh], ecx~%" +cons-tag+)
  (format t "	mov ecx, [esp+~2,'0xh]~%" +word-size+)
  (format t "	mov [eax+~2,'0xh], ecx~%" (- +word-size+ +cons-tag+)))

(defun gen-car ()
  (format t "	mov ecx, [esp]~%")
  (format t "	mov eax, [ecx-~2,'0xh]~%" +cons-tag+))

(defun gen-cdr ()
  (format t "	mov ecx, [esp]~%")
  (format t "	mov eax, [ecx+~2,'0xh]~%" (- +word-size+ +cons-tag+)))

(defun gen-setcar ()
  (format t "	mov eax, [esp]~%")
  (format t "	mov ecx, [esp+~2,'0xh]~%" +word-size+)
  (format t "	mov [eax-~2,'0xh], ecx~%" +cons-tag+))

(defun gen-setcdr ()
  (format t "	mov ecx, [esp]~%")
  (format t "	mov eax, [esp+~2,'0xh]~%" +word-size+)
  (format t "	mov [eax+~2,'0xh], ecx~%" (- +word-size+ +cons-tag+)))

(defun gen-consp ()
  (format t "	mov ecx, [esp]~%")
  (format t "	and ecx, ~2,'0xh~%" +non-immediate-mask+)
  (format t "	cmp ecx, ~2,'0xh~%" +cons-tag+)
  (format t "	sete al~%")
  (format t "	movzx eax, al~%")
  (format t "	sal eax, ~2,'0xh~%" +bool-shift+)
  (format t "	or eax, ~a~%" (gen-immediate nil)))

(defun gen-setup-funcall-arg-space (size)
  (format t "	sub esp, ~2,'0xh~%" (* +word-size+ size)))

(defun gen-cleanup-funcall-arg-space (size)
  (format t "	add esp, ~2,'0xh~%" (* +word-size+ size)))

(defun gen-load-funcall-arg (pos)
  (format t "	mov [esp+~2,'0xh], eax~%" (* +word-size+ pos)))

(defun gen-funcall (fun)
  (format t "	call ~a~%" fun))

(defun gen-lexenv-create (length)
  (format t "	push DWORD ~2,'0xh~%" length)
  (format t "	call alloc_lexenv~%")
  (format t "	add esp, ~2,'0xh~%" +word-size+)
  (format t "	mov [eax], esi~%")
  (format t "	mov esi, eax~%"))

(defun gen-lexenv-destroy ()
  ;; TODO: Don't just drop the pointer to the stale lexenv...
  (format t "	mov esi, [esi]~%"))

(defun gen-lexenv-store (position)
  (format t "	mov [esi+~2,'0xh], eax~%" (* (1+ position) +word-size+)))

(defun gen-lexenv-load (depth position)
  (format t "	mov eax, esi~%")
  (loop repeat depth do
       (format t "	mov eax, [eax]~%"))
  (format t "	mov eax, [eax+~2,'0xh]~%" (* (1+ position) +word-size+)))

(defun gen-function-prologue ()
  nil)

(defun gen-function-epilogue ()
  (format t "	ret~%~%"))

(defun gen-funcall (name)
  (format t "	call ~a~%" name))

(defun gen-ir-instruction (ir)
  (format t "	; ~a~%" ir)
  (destructuring-bind (inst &rest args) ir
    (ecase inst
      (:load-immediate (gen-load-immediate (first args)))
      (:jmp (gen-jmp (first args)))
      (:jmp-if-nil (gen-jmp-if-nil (first args)))
      (:jmp-if-t (gen-jmp-if-t (first args)))
      (:eq (gen-eq))
      (:null (gen-null))
      (:cons (gen-cons))
      (:car (gen-car))
      (:cdr (gen-cdr))
      (:setcar (gen-setcar))
      (:setcdr (gen-setcdr))
      (:consp (gen-consp))
      (:setup-funcall-arg-space (gen-setup-funcall-arg-space (first args)))
      (:cleanup-funcall-arg-space (gen-cleanup-funcall-arg-space (first args)))
      (:load-funcall-arg (gen-load-funcall-arg (first args)))
      (:funcall (gen-funcall (first args)))
      (:lexenv-create (gen-lexenv-create (first args)))
      (:lexenv-destroy (gen-lexenv-destroy))
      (:lexenv-store (gen-lexenv-store (first args)))
      (:lexenv-load (gen-lexenv-load (first args) (second args)))
      (:function-prologue (gen-function-prologue))
      (:function-epilogue (gen-function-epilogue)))))

(defun ir->asm (ir)
  (etypecase ir
    (string
     (format t "~a:~%" ir))
    (list
     (gen-ir-instruction ir))))

(defun irs->asm (irs)
  (if (listp (car irs))
      (mapc #'ir->asm irs)
      (ir->asm irs))
  nil)
