;; fsbl2 - homebrew Lisp system
;; Copyright (C) 2018 Robert Cochran <robert-git@cochranmail.com>
;;
;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;; TODO: probably ought to find a way to define a whole bunch of this stuff in
;; such a way that IR can make use of it and still have the ability for
;; different systems to have different sizes and representation of things

;; FIXME: lots of stuff defined both here and GEN-X86

(defpackage :ir
  (:use :common-lisp)
  (:export :form->ir
	   :forms->ir
	   :gen-builtins))

(in-package :ir)

(defconstant +fixnum-shift+ 2)
(defconstant +fixnum-mask+ #x03)

(defconstant +word-size+ 4)

(defconstant +fixnum-bits+ (- (* +word-size+ 8) +fixnum-shift+))
(defconstant +fixnum-min+ (- (expt 2 (1- +fixnum-bits+))))
(defconstant +fixnum-max+ (1- (expt 2 (1- +fixnum-bits+))))

(defparameter *core-builtin-list*
  '(:null :eq :cons :car :cdr :setcar :setcdr :consp))

(defparameter *label-num* 0
  "Unique label number")

(defun gen-label ()
  (prog1 (concatenate 'string ".label_" (write-to-string *label-num*))
    (incf *label-num*)))

(defmacro with-labels (label-list &body body)
  `(let ,(loop for l in label-list collecting `(,l (gen-label)))
     ,@body))

(defun list-merge (&rest items)
  (flet ((wrap-if-necessary (x)
	   (if (notany #'consp x)
	       (list x)
	       x)))
    (reduce #'append
	    (mapcar #'wrap-if-necessary
		    items))))

(defun immediatep (form)
  (or (eq form t)
      (null form)
      #+sbcl
      (typep form 'base-char)
      (and (integerp form)
	   (<= +fixnum-min+ form +fixnum-max+))))

(defun if-form-p (form)
  (and (listp form)
       (eq (first form) 'if)
       (<= 3 (length form) 4)))

(defun if->ir (form lexenvs)
  (with-labels (alt-label end-label)
    (list-merge (form->ir (second form) lexenvs)
		(list :jmp-if-nil alt-label)
		(form->ir (third form) lexenvs)
		(list :jmp end-label)
		alt-label
		(form->ir (fourth form) lexenvs)
		end-label)))

(defun let-form-p (form)
  (and (listp form)
       (eq (first form) 'let)))

(defun listify (x)
  (if (listp x)
      x
      (list x)))

(defun let->ir (form lexenvs)
  (destructuring-bind ((&rest binds) &rest body) (rest form)
    (let ((bind-list (mapcar #'listify binds)))
      (loop
	 for (nil value) in bind-list
	 for counter below (length binds)
	 collecting (list-merge (form->ir value lexenvs)
				(list :lexenv-store counter))
	 into values
	 finally (return (list-merge (list :lexenv-create (length binds))
				     (reduce #'list-merge values)
				     (forms->ir body (list bind-list lexenvs))
				     (list :lexenv-destroy (length binds))))))))

(defun cond-form-p (form)
  (and (listp form)
       (eq (first form) 'cond)))

(defun cond-sub-form->ir (cond-sub-form lexenvs current-label next-label end-label)
  (destructuring-bind (predicate &rest body) cond-sub-form
    (list-merge current-label
		(form->ir predicate lexenvs)
		(list :jmp-if-nil next-label)
		(forms->ir body lexenvs)
		(list :jmp end-label))))

(defun cond->ir (form lexenvs)
  (destructuring-bind (&rest cond-sub-forms) (rest form)
    (with-labels (end-label)
      (loop
	 for cond-sub-form in cond-sub-forms
	 for current-label = nil
	 then next-label
	 for next-label = (gen-label)
	 collect (cond-sub-form->ir lexenvs cond-sub-form current-label next-label end-label)
	 into cond-sub-form-irs
	 ;; REST here so that we skip the NIL initial CURRENT-LABEL
	 finally (return (rest (list-merge (reduce #'list-merge cond-sub-form-irs)
					   next-label
					   (list :load-immediate nil)
					   end-label)))))))

(defun progn-form-p (form)
  (and (listp form)
       (eq (first form) 'progn)))

(defun progn->ir (form lexenvs)
  (forms->ir (rest form) lexenvs))

(defun intrinsic-form-p (form)
  (and (listp form)
       (member (intern (symbol-name (first form)) 'keyword)
	       *core-builtin-list*)))

(defun intrinsic->ir (form lexenvs)
  (destructuring-bind (fun &rest args) form
    (list-merge (list :setup-funcall-arg-space (length args))
		(reduce #'list-merge
			(loop
			   for arg in args
			   for count below (length args)
			   collecting (list-merge (form->ir arg lexenvs)
						  (list :load-funcall-arg count))))
		(list (intern (symbol-name fun) 'keyword))
		(list :cleanup-funcall-arg-space (length args)))))

(defun lexenv-load->ir (form lexenvs)
  (labels ((lexenv-search (form lexenvs lexenv-depth)
	     (if (null lexenvs)
		 (error "~a not lexically defined" form)
		 (let ((lexenv-pos (position form (car lexenvs) :key #'car)))
		   (if lexenv-pos
		       (list :lexenv-load lexenv-depth lexenv-pos)
		       (lexenv-search form (second lexenvs) (1+ lexenv-depth)))))))
    (lexenv-search form lexenvs 0)))

(defun function-defined-p (form)
  (or (builtins:builtin-defined-p (car form))
      ;; TODO: User defined functions...
      ))

(defun funcall->ir (form lexenvs)
  (destructuring-bind (fun &rest args) form
    (list-merge (list :lexenv-create (length args))
		(reduce #'list-merge
			(loop
			   for arg in args
			   for count below (length args)
			   collecting (list-merge (form->ir arg lexenvs)
						  (list :lexenv-store count))))
		(list :funcall (concatenate 'string "fsbl_" (symbol-name fun)))
		(list :lexenv-destroy (length args)))))

(defun form->ir (form lexenvs)
  (cond ((immediatep form)
	 (list (list :load-immediate form)))
	((symbolp form)
	 (lexenv-load->ir form lexenvs))
	((if-form-p form)
	 (if->ir form lexenvs))
	((let-form-p form)
	 (let->ir form lexenvs))
	((cond-form-p form)
	 (cond->ir form lexenvs))
	((progn-form-p form)
	 (progn->ir form lexenvs))
	((intrinsic-form-p form)
	 (intrinsic->ir form lexenvs))
	((function-defined-p form)
	 (funcall->ir form lexenvs))
	(t
	 (error "Form is bad!"))))

(defun forms->ir (forms lexenvs)
  (reduce #'list-merge
	  (mapcar (lambda (x) (form->ir x lexenvs))
		  forms)))

(defun gen-builtins ()
  ;; TODO: Make a nicer accessor for the builtins list
  (first (loop for builtin in builtins::*builtins* collect
	      (list-merge (builtins:builtin-asm-name builtin)
			  (list :function-prologue)
			  (forms->ir (builtins:fsbl-builtin-fun-body builtin)
				     (list (mapcar (lambda (x) (list x nil))
						   (builtins:fsbl-builtin-fun-lambda-list builtin))))
			  (list :function-epilogue)))))
