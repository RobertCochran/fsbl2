;; fsbl2 - homebrew Lisp system
;; Copyright (C) 2018 Robert Cochran <robert-git@cochranmail.com>
;;
;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(defpackage :compiler
  (:use :common-lisp
	:sb-ext))

(in-package :compiler)

(ql:quickload "ir")
(ql:quickload "gen-x86")

(defun emit-program (forms)
  (format t "EXTERN alloc_cons~%")
  (format t "EXTERN alloc_lexenv~%")
  (format t "SECTION .text~%")
  (mapc #'gen-x86:ir->asm (ir:gen-builtins))
  (format t "GLOBAL fsbl_entry~%")
  (format t "fsbl_entry:~%")
  (format t "	; Lisp prologue~%")
  (gen-x86:gen-lisp-prologue)
  (gen-x86:irs->asm (ir:forms->ir forms nil))
  (format t "	; Lisp epilogue~%")
  (gen-x86:gen-lisp-epilogue))

(defun get-argv ()
  (or #+sbcl (cdr sb-ext:*posix-argv*)
      #+ecl ext:argv
      (error "Don't know how to get C argv on this Lisp!")))

(defun read-from-file (filename)
  (let ((end-sym (gensym)))
    (with-open-file (read-stream filename :if-does-not-exist :error)
      (loop for res = (read read-stream nil end-sym)
	 until (eq res end-sym)
         collect res))))

(defun write-output-to-file (file thunk)
  (with-open-file (*standard-output* file
                                     :direction :output
                                     :if-exists :supersede
                                     :if-does-not-exist :create)
    (funcall thunk)))

(defun main ()
  (let* ((argv (get-argv))
         (infile (first argv))
         (outfile (second argv)))
    (write-output-to-file outfile
                          (lambda ()
                            (emit-program (read-from-file infile))))))

(main)
(quit)
