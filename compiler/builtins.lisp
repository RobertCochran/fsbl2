;; fsbl2 - homebrew Lisp system
;; Copyright (C) 2018 Robert Cochran <robert-git@cochranmail.com>
;;
;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(defpackage :builtins
  (:use :common-lisp)
  (:export :fsbl-builtin-fun-name
	   :fsbl-builtin-fun-lambda-list
	   :fsbl-builtin-fun-body
	   :builtin-asm-name
	   :builtin-defined-p))

(in-package :builtins)

(defparameter *builtins*
  nil)

(defstruct fsbl-builtin-fun
  (name nil :type symbol)
  (lambda-list nil :type list)
  (body nil :type list))

(defun builtin-defined-p (name)
  (member name *builtins* :key #'fsbl-builtin-fun-name))

(defun builtin-asm-name (name)
  (concatenate 'string
	       "fsbl_"
	       (symbol-name (fsbl-builtin-fun-name name))))

(defmacro fsbl-defun (name lambda-list &body body)
  `(pushnew (make-fsbl-builtin-fun :name ',name
				   :lambda-list ',lambda-list
				   :body ',body)
	   *builtins*
	   :key #'fsbl-builtin-fun-name))

(fsbl-defun listp (x)
  (if (consp x)
      t
      (null x)))
