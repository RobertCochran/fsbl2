#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>

#define FIXNUM_SHIFT 2
#define FIXNUM_MASK 0x03

#define CHAR_SHIFT 8
#define CHAR_MASK 0x0f

#define NIL_REP 0x2f
#define T_REP 0x6f

#define NON_IMMEDIATE_MASK 0x07
#define CONS_MASK 0x01

#define UNUSED(x) ((void)x)

void *fsbl_entry(void);

void *alloc_cons()
{
	void *p = calloc(2, sizeof(uintptr_t));

	if (!p) {
		perror("cons allocation failed");
		exit(1);
	}

	/*
	 * Cheat really hard and make the basically always-valid assumption that
	 * the memory returned from malloc is word-or-better aligned and that we
	 * won't accidentally clobber our address by twiddling with the low
	 * bits.
	 */

	return (void *) (((uintptr_t) p) | CONS_MASK);
}

void *alloc_lexenv(uint32_t count)
{
	void *p = calloc(count + 1, sizeof(uintptr_t));

	if (!p) {
		perror("lexenv allocation failed");
		exit(1);
	}

	return p;
}

static inline int nilp(void *p)
{
	return ((uintptr_t) p) == NIL_REP;
}

static inline int tp(void *p)
{
	return ((uintptr_t) p) == T_REP;
}

static inline int fixnump(void *p)
{
	return (((uintptr_t) p) & FIXNUM_MASK) == 0;
}

static inline int charp(void *p)
{
	return (((uintptr_t) p) & CHAR_MASK) == CHAR_MASK;
}

static inline int consp(void *p)
{
	return (((uintptr_t) p) & NON_IMMEDIATE_MASK) == CONS_MASK;
}

static void *cons_car(void *p)
{
	return (void *) *((uintptr_t *) (((char *) p) - CONS_MASK));
}

static void *cons_cdr(void *p)
{
	return (void *) *((uintptr_t *) (((char *) p) + (sizeof(uintptr_t) - CONS_MASK)));
}

void print_fsbl_value(void *p)
{
	int val = (int) p;

	if (nilp(p)) {
		printf("nil");
	} else if (tp(p)) {
		printf("t");
	} else if (fixnump(p)) {
		printf("%d", val >> FIXNUM_SHIFT);
	} else if (charp(p)) {
		printf("#\\%c", (char)(val >> CHAR_SHIFT));
	} else if (consp(p)) {
		printf("(");

		void *tmp;
		for (tmp = p; ; tmp = cons_cdr(tmp)) {
			print_fsbl_value(cons_car(tmp));

			if (nilp(cons_cdr(tmp))) {
				break;
			} else if (consp(cons_cdr(tmp))) {
				printf(" ");
			} else {
				printf(" . ");
				print_fsbl_value(cons_cdr(tmp));
				break;
			}
		}

		printf(")");
	} else {
		fprintf(stderr, "Don't know how to print 0x%08x!\n", (unsigned int) val);
	}
}

int main(int argc, char **argv)
{
	UNUSED(argc);
	UNUSED(argv);
	print_fsbl_value(fsbl_entry());
	printf("\n");
	return 0;
}
