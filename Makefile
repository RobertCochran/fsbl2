# Makefile - build script

SOURCE_FILES := $(wildcard runtime/*.c)
OBJECT_FILES := $(patsubst %.c,%.o,$(SOURCE_FILES))

PROJECT := fsbl-program

# Build flags

INCLUDES    := -Iinclude

BASEFLAGS   := -O2 -m32

WARNFLAGS   := -Wall -Wextra -Wunused -Wshadow -Wcast-align -Winline
WARNFLAGS   += -Wno-attributes -Wno-endif-labels -Wfloat-equal -Wformat=2
WARNFLAGS   += -Winit-self -Wmissing-format-attribute -Wmissing-include-dirs
WARNFLAGS   += -Wundef -Wwrite-strings -Wdisabled-optimization -Wpointer-arith

CFLAGS      := $(INCLUDES) $(BASEFLAGS) $(WARNFLAGS)
ASMFLAGS    := -felf32

# build rules

all: release

release: $(OBJECT_FILES) runtime/fsbl-out.o
	$(CC) $(CFLAGS) -o $(PROJECT) $(OBJECT_FILES) runtime/fsbl-out.o

debug: CFLAGS += -Og -g
debug: ASMFLAGS += -gstabs
debug: PROJECT := $(PROJECT)-debug
debug: release

runtime/fsbl-out.s: program.fsbl
	sbcl --load compiler/compiler.lisp program.fsbl runtime/fsbl-out.s

runtime/fsbl-out.o: runtime/fsbl-out.s
	nasm $(ASMFLAGS) -o runtime/fsbl-out.o runtime/fsbl-out.s

clean:
	rm -rf $(PROJECT) $(PROJECT)-debug $(OBJECT_FILES) runtime/fsbl-out.o runtime/fsbl-out.s
